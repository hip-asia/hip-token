'use strict';

const Web3 = require('web3');
const web3 = new Web3("ws://localhost:8545");
const bN = web3.utils.toBN;
const l = console.log;
const Hip = artifacts.require('HipToken');

const chai = require('chai');
const BN = require('bn.js');
const bnChai = require('bn-chai');
chai.use(bnChai(BN));
chai.config.includeStack = true;
const expect = chai.expect;

async function assertEvents(promise, eventNames) {
    let rcpt = await promise;
    if (typeof (rcpt) === 'string') {
        rcpt = await web3.eth.getTransactionReceipt(rcpt);
    }

    assert(rcpt.logs.length === eventNames.length);
    for (let i = 0; i < rcpt.logs.length; i++) {
        assert(rcpt.logs[i].event === eventNames[i]);
    }
    return rcpt;
}

async function assertReverts(fxn, args) {
    try {
        await fxn(args);
        assert(false);
    } catch (e) {
        //
    }
}

async function increaseTime(bySeconds) {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_increaseTime',
        params: [bySeconds],
        id: new Date().getTime(),
    }, (err, result) => {
      if (err) { console.error(err) }

    });
    await mineOneBlock();
}

async function mineOneBlock() {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: new Date().getTime(),
    }, () => {});
}

contract('HipToken', function (accounts) {
    const state = {};

    beforeEach(async () => {
        state.centralBank = accounts[0];
        state.initialBalance = bN('100000000000000000000');
        state.hip = await Hip.new(state.centralBank, state.initialBalance);
    });

    afterEach(() => {});

    it('it allows transfer of coins', async function () {
        const amountTransfer = bN(1e18);
        const centralBankBalanceBefore = await state.hip.balanceOf(state.centralBank);
        const receiverBalanceBefore = await state.hip.balanceOf(accounts[1]);
        const totalSupplyBefore = await state.hip.totalSupply();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance);
        expect(receiverBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance);
        await assertEvents(state.hip.transfer(accounts[1], amountTransfer), ['Transfer']);
        await assertReverts(state.hip.transfer, [accounts[3], amountTransfer, {from: accounts[2]}]);
        const centralBankBalanceAfter = await state.hip.balanceOf(state.centralBank);
        const receiverBalanceAfter = await state.hip.balanceOf(accounts[1]);
        const totalSupplyAfter = await state.hip.totalSupply();
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        expect(receiverBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance);
    });

    it('it allows transferFrom and manipulation of allowance', async () => {
        let owner = state.centralBank;
        let spender = accounts[1];
        let receiver = accounts[2];
        expect(owner !== spender);
        let initialAllowance = await state.hip.allowance(owner, spender);
        expect(initialAllowance).to.be.zero;
        await assertReverts(state.hip.transferFrom, [owner, receiver, bN(1), {from: spender}]);
        await assertReverts(state.hip.approve, [spender, bN(1), {from: spender}]);
        await assertEvents(state.hip.approve(spender, bN(1), {from: owner}), ['Approval']);
        let allowanceAfter = await state.hip.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(1);
        await assertReverts(state.hip.transferFrom, [owner, receiver, bN(2), {from: spender}]);
        allowanceAfter = await state.hip.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(1);
        await assertEvents(state.hip.transferFrom(owner, receiver, bN(1), {from: spender}), ['Transfer']);
        allowanceAfter = await state.hip.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(0);
        await assertEvents(state.hip.decreaseApproval(spender, bN(2), {from: owner}), ['Approval']);
        allowanceAfter = await state.hip.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(0);
        await assertEvents(state.hip.increaseApproval(spender, bN(2), {from: owner}), ['Approval']);
        allowanceAfter = await state.hip.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(2);
        await assertEvents(state.hip.increaseApproval(spender, bN(3), {from: owner}), ['Approval']);
        allowanceAfter = await state.hip.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(5);
        await assertEvents(state.hip.transferFrom(owner, receiver, bN(1), {from: spender}), ['Transfer']);
        allowanceAfter = await state.hip.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(4);
        await assertEvents(state.hip.decreaseApproval(spender, bN(2), {from: owner}), ['Approval']);
        allowanceAfter = await state.hip.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(2);
        await assertEvents(state.hip.transferFrom(owner, receiver, bN(2), {from: spender}), ['Transfer']);
        allowanceAfter = await state.hip.allowance(owner, spender);
        expect(allowanceAfter).to.eq.BN(0);


    });


    it('it allows freezing of coins', async function () {
        const amountTransfer = bN(1e18);
        const centralBankBalanceBefore = await state.hip.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceBefore = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyBefore = await state.hip.totalSupply();
        const numCoinsFrozenBefore = await state.hip.numCoinsFrozen();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance);
        // expect(centralBankFrozenBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance);
        expect(numCoinsFrozenBefore).to.be.zero;
        await assertEvents(state.hip.freeze(amountTransfer, 1), ['Transfer', 'TokensFrozen']);
        const centralBankBalanceAfter = await state.hip.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceAfter = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyAfter = await state.hip.totalSupply();
        const numCoinsFrozenAfter = await state.hip.numCoinsFrozen();
        const frozenStruct = await state.hip.frozenTokensMap(bN(1));
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        // expect(centralBankFrozenBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance);
        expect(numCoinsFrozenAfter).to.eq.BN(amountTransfer);
        expect(frozenStruct.id).to.eq.BN(1);
        expect(frozenStruct.dateFrozen).to.not.be.zero;
        expect(frozenStruct.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStruct.amount).to.eq.BN(amountTransfer);
        expect(frozenStruct.frozen).to.be.true;
        expect(frozenStruct.owner).to.be.a('string').that.equals(state.centralBank);
    });

    it('it disallows transfer of frozen coins', async function () {
        const amountTransfer = await state.hip.totalSupply();
        const centralBankBalanceBefore = await state.hip.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceBefore = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyBefore = await state.hip.totalSupply();
        const numCoinsFrozenBefore = await state.hip.numCoinsFrozen();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance);
        // expect(centralBankFrozenBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance);
        expect(numCoinsFrozenBefore).to.be.zero;
        await assertEvents(state.hip.freeze(amountTransfer, 1), ['Transfer', 'TokensFrozen']);
        const centralBankBalanceAfter = await state.hip.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceAfter = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyAfter = await state.hip.totalSupply();
        const numCoinsFrozenAfter = await state.hip.numCoinsFrozen();
        const frozenStruct = await state.hip.frozenTokensMap(bN(1));
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        // expect(centralBankFrozenBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance);
        expect(numCoinsFrozenAfter).to.eq.BN(amountTransfer);
        expect(frozenStruct.id).to.eq.BN(1);
        expect(frozenStruct.dateFrozen).to.not.be.zero;
        expect(frozenStruct.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStruct.amount).to.eq.BN(amountTransfer);
        expect(frozenStruct.frozen).to.be.true;
        expect(frozenStruct.owner).to.be.a('string').that.equals(state.centralBank);
        assertReverts(state.hip.transfer, [bN(1), {from: state.centralBank}]);
        assertReverts(state.hip.transfer, [bN(1000000000000000), {from: state.centralBank}]);
    });

    it('it disallows only transfer of frozen coins, unfrozen transfer allowed', async function () {
        const amountTransfer = state.initialBalance.div(bN(2));
        const centralBankBalanceBefore = await state.hip.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceBefore = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyBefore = await state.hip.totalSupply();
        const numCoinsFrozenBefore = await state.hip.numCoinsFrozen();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance);
        // expect(centralBankFrozenBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance);
        expect(numCoinsFrozenBefore).to.be.zero;
        await assertEvents(state.hip.freeze(amountTransfer, 1), ['Transfer', 'TokensFrozen']);
        const centralBankBalanceAfter = await state.hip.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceAfter = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyAfter = await state.hip.totalSupply();
        const numCoinsFrozenAfter = await state.hip.numCoinsFrozen();
        const frozenStruct = await state.hip.frozenTokensMap(bN(1));
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        // expect(centralBankFrozenBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance);
        expect(numCoinsFrozenAfter).to.eq.BN(amountTransfer);
        expect(frozenStruct.id).to.eq.BN(1);
        expect(frozenStruct.dateFrozen).to.not.be.zero;
        expect(frozenStruct.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStruct.amount).to.eq.BN(amountTransfer);
        expect(frozenStruct.frozen).to.be.true;
        expect(frozenStruct.owner).to.be.a('string').that.equals(state.centralBank);
        await assertEvents(state.hip.transfer(accounts[1], amountTransfer, {from: state.centralBank}), ['Transfer']);
        assertReverts(state.hip.transfer, [bN(1), {from: state.centralBank}]);
        assertReverts(state.hip.transfer, [bN(1000000000000000), {from: state.centralBank}]);
    });

    it('it allows unfreezing after timelimit', async function () {
        const amountTransfer = state.initialBalance.div(bN(2));
        const centralBankBalanceBefore = await state.hip.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceBefore = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyBefore = await state.hip.totalSupply();
        const numCoinsFrozenBefore = await state.hip.numCoinsFrozen();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance);
        // expect(centralBankFrozenBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance);
        expect(numCoinsFrozenBefore).to.be.zero;
        await assertEvents(state.hip.freeze(amountTransfer, 1, {from: state.centralBank}), ['Transfer', 'TokensFrozen']);
        const centralBankBalanceAfter = await state.hip.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceAfter = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyAfter = await state.hip.totalSupply();
        const numCoinsFrozenAfter = await state.hip.numCoinsFrozen();
        const frozenStructBefore = await state.hip.frozenTokensMap(bN(1));
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        // expect(centralBankFrozenBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance);
        expect(numCoinsFrozenAfter).to.eq.BN(amountTransfer);
        expect(frozenStructBefore.id).to.eq.BN(1);
        expect(frozenStructBefore.dateFrozen).to.not.be.zero;
        expect(frozenStructBefore.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStructBefore.amount).to.eq.BN(amountTransfer);
        expect(frozenStructBefore.frozen).to.be.true;
        expect(frozenStructBefore.owner).to.be.a('string').that.equals(state.centralBank);
        assertReverts(state.hip.unFreeze, [bN(1), {from: state.centralBank}]);
        await increaseTime(60 * 60 * 24 + 1);
        await assertEvents(state.hip.unFreeze(bN(1), {from: state.centralBank}), ['Transfer', 'TokensUnfrozen']);
        const frozenStructAfter = await state.hip.frozenTokensMap(bN(1));
        expect(frozenStructAfter.id).to.eq.BN(1);
        expect(frozenStructAfter.dateFrozen).to.not.be.zero;
        expect(frozenStructAfter.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStructAfter.amount).to.eq.BN(amountTransfer);
        expect(frozenStructAfter.frozen).to.be.false;
        expect(frozenStructAfter.owner).to.be.a('string').that.equals(state.centralBank);
    });

    it('it allows transfer after unfreezing', async function () {
        const amountTransfer = state.initialBalance;
        const centralBankBalanceBefore = await state.hip.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceBefore = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyBefore = await state.hip.totalSupply();
        const numCoinsFrozenBefore = await state.hip.numCoinsFrozen();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance);
        // expect(centralBankFrozenBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance);
        expect(numCoinsFrozenBefore).to.be.zero;
        await assertEvents(state.hip.freeze(amountTransfer, 1, {from: state.centralBank}), ['Transfer', 'TokensFrozen']);
        const centralBankBalanceAfter = await state.hip.balanceOf(state.centralBank);
        // const centralBankFrozenBalanceAfter = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyAfter = await state.hip.totalSupply();
        const numCoinsFrozenAfter = await state.hip.numCoinsFrozen();
        const frozenStruct = await state.hip.frozenTokensMap(bN(1));
        expect(centralBankBalanceBefore.sub(centralBankBalanceAfter)).to.eq.BN(amountTransfer);
        // expect(centralBankFrozenBalanceAfter).to.eq.BN(amountTransfer);
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance);
        expect(numCoinsFrozenAfter).to.eq.BN(amountTransfer);
        expect(frozenStruct.id).to.eq.BN(1);
        expect(frozenStruct.dateFrozen).to.not.be.zero;
        expect(frozenStruct.lengthFreezeDays).to.eq.BN(1);
        expect(frozenStruct.amount).to.eq.BN(amountTransfer);
        expect(frozenStruct.frozen).to.be.true;
        expect(frozenStruct.owner).to.be.a('string').that.equals(state.centralBank);
        await increaseTime(60 * 60 * 24 + 1);
        await assertEvents(state.hip.unFreeze(bN(1), {from: state.centralBank}), ['Transfer', 'TokensUnfrozen']);
        await assertEvents(state.hip.transfer(accounts[1], amountTransfer, {from: state.centralBank}), ['Transfer']);
    });

    it('it allows multiple simultaneous freezings', async () => {
        const freezeThese1 = bN(1e18);
        const freezeThese2 = bN(1e19);
        const freezeThese3 = bN(2e18);
        const numCoinsFrozenBefore = await state.hip.numCoinsFrozen();
        const totalSupplyBefore = await state.hip.totalSupply();
        // const frozenBalance1Before = await state.hip.frozenBalanceOf(state.centralBank);
        // const frozenBalance2Before = await state.hip.frozenBalanceOf(accounts[1]);
        // const frozenBalance3Before = await state.hip.frozenBalanceOf(accounts[2]);
        await state.hip.transfer(accounts[1], freezeThese2, {from:state.centralBank});
        await state.hip.transfer(accounts[2], freezeThese3, {from:state.centralBank});
        const balance1Before = await state.hip.balanceOf(state.centralBank);
        const balance2Before = await state.hip.balanceOf(accounts[1]);
        const balance3Before = await state.hip.balanceOf(accounts[2]);


        await assertEvents(state.hip.freeze(freezeThese1, 1, {from: state.centralBank}), ['Transfer', 'TokensFrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter1 = await state.hip.numCoinsFrozen();
        const totalSupplyAfter1 = await state.hip.totalSupply();
        // const frozenBalance1After1 = await state.hip.frozenBalanceOf(state.centralBank);
        // const frozenBalance2After1 = await state.hip.frozenBalanceOf(accounts[1]);
        // const frozenBalance3After1 = await state.hip.frozenBalanceOf(accounts[2]);
        const balance1After1 = await state.hip.balanceOf(state.centralBank);
        const balance2After1 = await state.hip.balanceOf(accounts[1]);
        const balance3After1 = await state.hip.balanceOf(accounts[2]);

        await assertEvents(state.hip.freeze(freezeThese2, 1, {from: accounts[1]}), ['Transfer', 'TokensFrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter2 = await state.hip.numCoinsFrozen();
        const totalSupplyAfter2 = await state.hip.totalSupply();
        // const frozenBalance1After2 = await state.hip.frozenBalanceOf(state.centralBank);
        // const frozenBalance2After2 = await state.hip.frozenBalanceOf(accounts[1]);
        // const frozenBalance3After2 = await state.hip.frozenBalanceOf(accounts[2]);
        const balance1After2 = await state.hip.balanceOf(state.centralBank);
        const balance2After2 = await state.hip.balanceOf(accounts[1]);
        const balance3After2 = await state.hip.balanceOf(accounts[2]);

        await assertEvents(state.hip.freeze(freezeThese3, 1, {from: accounts[2]}), ['Transfer', 'TokensFrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter3 = await state.hip.numCoinsFrozen();
        const totalSupplyAfter3 = await state.hip.totalSupply();
        // const frozenBalance1After3 = await state.hip.frozenBalanceOf(state.centralBank);
        // const frozenBalance2After3 = await state.hip.frozenBalanceOf(accounts[1]);
        // const frozenBalance3After3 = await state.hip.frozenBalanceOf(accounts[2]);
        const balance1After3 = await state.hip.balanceOf(state.centralBank);
        const balance2After3 = await state.hip.balanceOf(accounts[1]);
        const balance3After3 = await state.hip.balanceOf(accounts[2]);

        await assertEvents(state.hip.unFreeze(bN(1), {from: accounts[0]}), ['Transfer', 'TokensUnfrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter4 = await state.hip.numCoinsFrozen();
        const totalSupplyAfter4 = await state.hip.totalSupply();
        // const frozenBalance1After4 = await state.hip.frozenBalanceOf(state.centralBank);
        // const frozenBalance2After4 = await state.hip.frozenBalanceOf(accounts[1]);
        // const frozenBalance3After4 = await state.hip.frozenBalanceOf(accounts[2]);
        const balance1After4 = await state.hip.balanceOf(state.centralBank);
        const balance2After4 = await state.hip.balanceOf(accounts[1]);
        const balance3After4 = await state.hip.balanceOf(accounts[2]);

        await assertEvents(state.hip.unFreeze(bN(3), {from: accounts[0]}), ['Transfer', 'TokensUnfrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter5 = await state.hip.numCoinsFrozen();
        const totalSupplyAfter5 = await state.hip.totalSupply();
        // const frozenBalance1After5 = await state.hip.frozenBalanceOf(state.centralBank);
        // const frozenBalance2After5 = await state.hip.frozenBalanceOf(accounts[1]);
        // const frozenBalance3After5 = await state.hip.frozenBalanceOf(accounts[2]);
        const balance1After5 = await state.hip.balanceOf(state.centralBank);
        const balance2After5 = await state.hip.balanceOf(accounts[1]);
        const balance3After5 = await state.hip.balanceOf(accounts[2]);

        await assertEvents(state.hip.unFreeze(bN(2), {from: accounts[0]}), ['Transfer', 'TokensUnfrozen']);
        await increaseTime(60 * 60 * 24 + 1);
        const numCoinsFrozenAfter6 = await state.hip.numCoinsFrozen();
        const totalSupplyAfter6 = await state.hip.totalSupply();
        // const frozenBalance1After6 = await state.hip.frozenBalanceOf(state.centralBank);
        // const frozenBalance2After6 = await state.hip.frozenBalanceOf(accounts[1]);
        // const frozenBalance3After6 = await state.hip.frozenBalanceOf(accounts[2]);
        const balance1After6 = await state.hip.balanceOf(state.centralBank);
        const balance2After6 = await state.hip.balanceOf(accounts[1]);
        const balance3After6 = await state.hip.balanceOf(accounts[2]);

        expect(numCoinsFrozenBefore).to.eq.BN(0);
        expect(numCoinsFrozenAfter1).to.eq.BN(freezeThese1);
        expect(numCoinsFrozenAfter2).to.eq.BN(freezeThese1.add(freezeThese2));
        expect(numCoinsFrozenAfter3).to.eq.BN(freezeThese1.add(freezeThese2).add(freezeThese3));
        expect(numCoinsFrozenAfter4).to.eq.BN(freezeThese2.add(freezeThese3));
        expect(numCoinsFrozenAfter5).to.eq.BN(freezeThese2);
        expect(numCoinsFrozenAfter6).to.eq.BN(0);

        expect(totalSupplyAfter1).to.eq.BN(totalSupplyBefore);
        expect(totalSupplyAfter2).to.eq.BN(totalSupplyBefore);
        expect(totalSupplyAfter3).to.eq.BN(totalSupplyBefore);
        expect(totalSupplyAfter4).to.eq.BN(totalSupplyBefore);
        expect(totalSupplyAfter5).to.eq.BN(totalSupplyBefore);
        expect(totalSupplyAfter6).to.eq.BN(totalSupplyBefore);

        // expect(frozenBalance1Before).to.eq.BN(0);
        // expect(frozenBalance1After1).to.eq.BN(freezeThese1);
        // expect(frozenBalance1After2).to.eq.BN(freezeThese1);
        // expect(frozenBalance1After3).to.eq.BN(freezeThese1);
        // expect(frozenBalance1After4).to.eq.BN(0);
        // expect(frozenBalance1After5).to.eq.BN(0);
        // expect(frozenBalance1After6).to.eq.BN(0);
        //
        // expect(frozenBalance2Before).to.eq.BN(0);
        // expect(frozenBalance2After1).to.eq.BN(0);
        // expect(frozenBalance2After2).to.eq.BN(freezeThese2);
        // expect(frozenBalance2After3).to.eq.BN(freezeThese2);
        // expect(frozenBalance2After4).to.eq.BN(freezeThese2);
        // expect(frozenBalance2After5).to.eq.BN(freezeThese2);
        // expect(frozenBalance2After6).to.eq.BN(0);
        //
        // expect(frozenBalance3Before).to.eq.BN(0);
        // expect(frozenBalance3After1).to.eq.BN(0);
        // expect(frozenBalance3After2).to.eq.BN(0);
        // expect(frozenBalance3After3).to.eq.BN(freezeThese3);
        // expect(frozenBalance3After4).to.eq.BN(freezeThese3);
        // expect(frozenBalance3After5).to.eq.BN(0);
        // expect(frozenBalance3After6).to.eq.BN(0);

        expect(balance1After1).to.eq.BN(balance1Before.sub(freezeThese1));
        expect(balance1After2).to.eq.BN(balance1Before.sub(freezeThese1));
        expect(balance1After3).to.eq.BN(balance1Before.sub(freezeThese1));
        expect(balance1After4).to.eq.BN(balance1Before);
        expect(balance1After5).to.eq.BN(balance1Before);
        expect(balance1After6).to.eq.BN(balance1Before);

        expect(balance2After1).to.eq.BN(balance2Before);
        expect(balance2After2).to.eq.BN(balance2Before.sub(freezeThese2));
        expect(balance2After3).to.eq.BN(balance2Before.sub(freezeThese2));
        expect(balance2After4).to.eq.BN(balance2Before.sub(freezeThese2));
        expect(balance2After5).to.eq.BN(balance2Before.sub(freezeThese2));
        expect(balance2After6).to.eq.BN(balance2Before);

        expect(balance3After1).to.eq.BN(balance3Before);
        expect(balance3After2).to.eq.BN(balance3Before);
        expect(balance3After3).to.eq.BN(balance3Before.sub(freezeThese3));
        expect(balance3After4).to.eq.BN(balance3Before.sub(freezeThese3));
        expect(balance3After5).to.eq.BN(balance3Before);
        expect(balance3After6).to.eq.BN(balance3Before);

    });

    it('it allows anyone to burn their tokens', async () => {
        let sender = state.centralBank;
        let burnThese = state.initialBalance.div(bN(3));
        let tooMany = state.initialBalance;
        let balanceBefore = await state.hip.balanceOf(sender);
        let totalSupplyBefore = await state.hip.totalSupply();
        await assertEvents(state.hip.burn(burnThese, {from: sender}), ['Transfer', 'TokensBurned']);
        let balanceAfter1 = await state.hip.balanceOf(sender);
        let totalSupplyAfter1 = await state.hip.totalSupply();
        await assertReverts(state.hip.burn, [tooMany, {from: sender}]);
        let balanceAfter2 = await state.hip.balanceOf(sender);
        let totalSupplyAfter2 = await state.hip.totalSupply();

        expect(balanceBefore).to.eq.BN(state.initialBalance);
        expect(balanceAfter1).to.eq.BN(balanceBefore.sub(burnThese));
        expect(balanceAfter2).to.eq.BN(balanceAfter1);
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance);
        expect(totalSupplyAfter1).to.eq.BN(totalSupplyBefore.sub(burnThese));
        expect(totalSupplyAfter2).to.eq.BN(totalSupplyAfter1);

        await state.hip.transfer(accounts[1], burnThese, {from: state.centralBank});
        sender = accounts[1];
        balanceBefore = await state.hip.balanceOf(sender);
        burnThese = balanceBefore.div(bN(3));
        tooMany = balanceBefore;
        totalSupplyBefore = await state.hip.totalSupply();
        await assertEvents(state.hip.burn(burnThese, {from: sender}), ['Transfer', 'TokensBurned']);
        balanceAfter1 = await state.hip.balanceOf(sender);
        totalSupplyAfter1 = await state.hip.totalSupply();
        await assertReverts(state.hip.burn, [tooMany, {from: sender}]);
        balanceAfter2 = await state.hip.balanceOf(sender);
        totalSupplyAfter2 = await state.hip.totalSupply();

        expect(balanceAfter1).to.eq.BN(balanceBefore.sub(burnThese));
        expect(balanceAfter2).to.eq.BN(balanceAfter1);
        expect(totalSupplyAfter1).to.eq.BN(totalSupplyBefore.sub(burnThese));
        expect(totalSupplyAfter2).to.eq.BN(totalSupplyAfter1);

    });

    it('it allows central bank to mint tokens', async function () {
        const amountMint = bN('10000000000000000000');
        const centralBankBalanceBefore = await state.hip.balanceOf(state.centralBank);
        const centralBankFrozenBalanceBefore = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyBefore = await state.hip.totalSupply();
        const maxSupplyBefore = await state.hip.maxSupply();
        expect(centralBankBalanceBefore).to.eq.BN(state.initialBalance);
        expect(centralBankFrozenBalanceBefore).to.be.zero;
        expect(totalSupplyBefore).to.eq.BN(state.initialBalance);
        expect(maxSupplyBefore).to.eq.BN(state.maxBalance);
        assertReverts(state.hip.mint, [amountMint, {from: accounts[1]}]);
        await assertEvents(state.hip.mint(amountMint, {from: state.centralBank}), ['CoinsMinted']);
        const centralBankBalanceAfter = await state.hip.balanceOf(state.centralBank);
        const centralBankFrozenBalanceAfter = await state.hip.frozenBalanceOf(state.centralBank);
        const totalSupplyAfter = await state.hip.totalSupply();
        const maxSupplyAfter = await state.hip.maxSupply();
        expect(centralBankBalanceAfter).to.eq.BN(state.initialBalance.add(amountMint));
        expect(centralBankFrozenBalanceAfter).to.be.zero;
        expect(totalSupplyAfter).to.eq.BN(state.initialBalance.add(amountMint));
        expect(maxSupplyAfter).to.eq.BN(state.maxBalance);
    });


});