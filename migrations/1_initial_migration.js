'use strict';

const SafeMath = artifacts.require('SafeMathLib');
const Hip = artifacts.require('HipToken');

const initialBalance = '10000000000000000000000';
const maxSupply = '1000000000000000000000000';

module.exports = async function(deployer, network, accounts) {
  deployer.deploy(SafeMath, {gasPrice: 15e9});
  deployer.link(SafeMath, Hip);
  deployer.deploy(Hip, accounts[0], initialBalance, {gasPrice: 15e9});
};
