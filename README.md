<h1> HIP Token </h1>

This is the token for HiP Lending. It allows freezing, burning, and of course transferring. Other than that, it's a pretty standard token. The magic happens in what the exchange does when the tokens are frozen.
<br><br><br>
<br>SafeMathLib: 0x951AED5E3554332BC2624D988c9c70d002D3Dba0
<br>HipToken: 0x9a8070492770EA7f1Ab232b789563a99008AeF76 
<br>HipToken.bank: 0xB793faF25BfdC5214D26111A7741E48a81AF1bD8
